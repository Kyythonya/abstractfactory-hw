﻿using System;

namespace AbstractFactoryHW
{
    public interface IComponent
    {
        string GetDescription();
    }

    public interface IComponentFirst: IComponent
    {
    }

    public interface IComponentSecond: IComponent
    {
        string GetComponentType(IComponentFirst componentFirst);
    }

    public class Accumulator : IComponentFirst
    {
        public string GetDescription()
        {
            return "Accumulator's description";
        }
    }

    public class Display : IComponentSecond
    {
        public string GetDescription()
        {
            return "Display's description";
        }

        public string GetComponentType(IComponentFirst componentFirst)
        {
            if (componentFirst is Accumulator)
                return "Accumulator's type";
            return "";
        }
    }


    public class Engine : IComponentFirst
    {
        public string GetDescription()
        {
            return "Engine's description";
        }
    }

    public class Body : IComponentSecond
    {
        public string GetComponentType(IComponentFirst componentFirst)
        {
            if (componentFirst is Engine)
                return "Engine's type";
            return "";
        }

        public string GetDescription()
        {
            return "Body's description";
        }
    }


    public class Processor : IComponentFirst
    {
        public string GetDescription()
        {
            return "Processor's description";
        }
    }

    public class Mainboard : IComponentSecond
    {
        public string GetComponentType(IComponentFirst componentFirst)
        {
            if (componentFirst is Processor)
                return "Processor's type";
            return "";
        }

        public string GetDescription()
        {
            return "Mainboard's description";
        }
    }

    public interface IManufacturer
    {
        IComponentFirst CreateComponentFirst();
        IComponentSecond CreateComponentSecond();
    }

    public class Samsung : IManufacturer
    {
        public IComponentFirst CreateComponentFirst()
        {
            return new Accumulator();
        }

        public IComponentSecond CreateComponentSecond()
        {
            return new Display();
        }
    }

    public class Nokia: IManufacturer
    {
        public IComponentFirst CreateComponentFirst()
        {
            return new Accumulator();
        }

        public IComponentSecond CreateComponentSecond()
        {
            return new Display();
        }
    }


    public class Ford : IManufacturer
    {
        public IComponentFirst CreateComponentFirst()
        {
            return new Engine();
        }

        public IComponentSecond CreateComponentSecond()
        {
            return new Body();
        }
    }

    public class Toyota : IManufacturer
    {
        public IComponentFirst CreateComponentFirst()
        {
            return new Engine();
        }

        public IComponentSecond CreateComponentSecond()
        {
            return new Body();
        }
    }


    public class Dell : IManufacturer
    {
        public IComponentFirst CreateComponentFirst()
        {
            return new Processor();
        }

        public IComponentSecond CreateComponentSecond()
        {
            return new Mainboard();
        }
    }

    public class Sony : IManufacturer
    {
        public IComponentFirst CreateComponentFirst()
        {
            return new Processor();
        }

        public IComponentSecond CreateComponentSecond()
        {
            return new Mainboard();
        }
    }

    public class Program
    {
        public static void Main(string[] args)
        {
            var samsungManufacturer = new Samsung();

            var samsungAccumulator = samsungManufacturer.CreateComponentFirst();
            var samsungDisplay = samsungManufacturer.CreateComponentSecond();

            Console.WriteLine($"Samsung's accumulator description: {samsungAccumulator.GetDescription()}\n" +
                                $"Samsung's display description: {samsungDisplay.GetDescription()}\n" +
                                $"Samsung's accumulator type: {samsungDisplay.GetComponentType(samsungAccumulator)}");

            Console.WriteLine("---------------------------------------------------------------------------------");

            var toyotaManufacturer = new Toyota();

            var toyotaEngine = toyotaManufacturer.CreateComponentFirst();
            var toyotaBody = toyotaManufacturer.CreateComponentSecond();

            Console.WriteLine($"Toyota's engine description: {toyotaEngine.GetDescription()}\n" +
                                $"Toyota's body description: {toyotaBody.GetDescription()}\n" +
                                $"Toyota's engine type: {toyotaBody.GetComponentType(toyotaEngine)}");

            Console.WriteLine("---------------------------------------------------------------------------------");

            var sonyManufacturer = new Sony();

            var sonyProcessor = sonyManufacturer.CreateComponentFirst();
            var sonyMainboard = sonyManufacturer.CreateComponentSecond();

            Console.WriteLine($"Sony's processor description: {sonyProcessor.GetDescription()}\n" +
                                $"Sony's mainboard description: {sonyMainboard.GetDescription()}\n" +
                                $"Sony's processor type: {sonyMainboard.GetComponentType(sonyProcessor)}");
            
            Console.ReadLine();
        }
    }
}
